'use strict';

var path = require('path');
var glob = require('glob');
var webpack = require('sgmf-scripts').webpack;
var ExtractTextPlugin = require('sgmf-scripts')['extract-text-webpack-plugin'];

var bootstrapPackages = {
    Alert: 'exports-loader?Alert!bootstrap/js/src/alert',
    // Button: 'exports-loader?Button!bootstrap/js/src/button',
    Carousel: 'exports-loader?Carousel!bootstrap/js/src/carousel',
    Collapse: 'exports-loader?Collapse!bootstrap/js/src/collapse',
    // Dropdown: 'exports-loader?Dropdown!bootstrap/js/src/dropdown',
    Modal: 'exports-loader?Modal!bootstrap/js/src/modal',
    // Popover: 'exports-loader?Popover!bootstrap/js/src/popover',
    Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/src/scrollspy',
    Tab: 'exports-loader?Tab!bootstrap/js/src/tab',
    // Tooltip: 'exports-loader?Tooltip!bootstrap/js/src/tooltip',
    Util: 'exports-loader?Util!bootstrap/js/src/util'
};

var cartridgeName = 'app_training_sfra';
var entryJsFiles = {};
var entryScssFiles = {};
const cartridgesPath = path.resolve(__dirname, "cartridges");
const clientPath = path.resolve(cartridgesPath, cartridgeName, "cartridge/client");

console.log(cartridgesPath, clientPath)

glob.sync(path.resolve(clientPath, "*", "js", "*.js")).forEach(f => {
    console.log('File:', f);
    const key = path.join(path.dirname(path.relative(clientPath, f)), path.basename(f, ".js"));
    entryJsFiles[key] = f;
});

glob.sync(path.resolve(clientPath, "*", "css", "**", "*.scss"))
    .filter(f => !path.basename(f).startsWith("_"))
    .forEach(f => {
        const key = path.join(path.dirname(path.relative(clientPath, f)), path.basename(f, ".scss"));
        entryScssFiles[key] = f;
    });

    console.log('ENTRY JS', entryJsFiles);
    console.log('ENTRY scss', entryScssFiles);

module.exports = [{
    mode: 'production',
    name: 'js',
    entry: entryJsFiles,
    output: {
        path: path.resolve(cartridgesPath, cartridgeName, 'cartridge/static'),
        filename: '[name].js'
    },
    resolve: {
        alias: {
            base: path.resolve(__dirname, "cartridges/app_storefront_base/cartridge/client/default/js")
        }
    },
    module: {
        rules: [
            {
                test: /bootstrap(.)*\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                        plugins: ['@babel/plugin-proposal-object-rest-spread'],
                        cacheDirectory: true
                    }
                }
            }
        ]
    },
    plugins: [new webpack.ProvidePlugin(bootstrapPackages)]
}, {
    mode: 'none',
    name: 'scss',
    entry: entryScssFiles,
    output: {
        path: path.resolve(cartridgesPath, cartridgeName, 'cartridge/static'),
        filename: '[name].css'
    },
    resolve: {
        alias: {
            base: path.resolve(__dirname, "cartridges/app_storefront_base/cartridge/client/default/scss")
        }
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                use: [{
                    loader: 'css-loader',
                    options: {
                        url: false,
                        minimize: true
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            require('autoprefixer')()
                        ]
                    }
                }, {
                    loader: 'sass-loader',
                    options: {
                        includePaths: [
                            path.resolve('node_modules'),
                            path.resolve('node_modules/flag-icon-css/sass')
                        ]
                    }
                }]
            })
        }]
    },
    plugins: [
        new ExtractTextPlugin({ filename: '[name].css' })
    ]
}];
